# arduino-examples

arduino examples and modelling with fritzing

#####folder structure of project

```
	|-- examples
		|-- 00-project-name
			|-- project-name
				|-- project-name.ino
			|-- README.md
			|-- assests
				|-- project-name.png
		|-- 01-
		...
```

### LICENSE

[The MIT License]


[The MIT License]: https://github.com/arslanbilal/arduino-examples/blob/master/LICENSE